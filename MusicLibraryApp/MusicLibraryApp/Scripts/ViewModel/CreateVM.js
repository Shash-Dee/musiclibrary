﻿var urlPath = window.location.pathname;
var CreateMusicLibraryVM = {
    Title: ko.observable(),
    Artist: ko.observable(),
    ReleaseDate: ko.observable(),
    Genre: ko.observable(),
    AlbumSize: ko.observable(),
    btnCreateLibrary: function () {

        $.ajax({
            url: urlPath,
            type: 'post',
            dataType: 'json',
            data: ko.toJSON(this),
            contentType: 'application/json',
            success: function (result) {
                window.location.href = urlPath + '/';
            },
            error: function (err) {
                if (err.responseText == "success") { window.location.href = urlPath + '/'; }
                else {
                    alert(err.responseText);
                }
            },
            complete: function () {
            }
        });

    }
};

ko.applyBindings(CreateMusicLibraryVM);