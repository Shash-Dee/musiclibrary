﻿var urlPath = window.location.pathname;

$(function ()
{
    ko.applyBindings(indexVM);
    indexVM.loadMusicLibraries();
});

var indexVM =
{
    MusicLibraries: ko.observableArray([]),

    loadMusicLibraries: function ()
    {
        var self = this;
        //Ajax Call Get All Music Libraries
        $.ajax({
            type: "GET",
            url: urlPath + '/FillIndex',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data)
            {
                self.MusicLibraries(data);
            },
            error: function (err)
            {
                alert(err.status + " : " + err.statusText);
            }
        });

    },

    editLibrary: function (data)
    {
        window.location.href = urlPath + '/Edit/' + data.ID;
    },

    detailsLibrary: function (data)
    {
        window.location.href = urlPath + '/Details/' + data.ID;
    },

    deleteLibrary: function (data)
    {
        window.location.href = urlPath + '/Delete/' + data.ID;
    }
};

function MusicLibraries(MusicLibraries)
{
    this.ID = ko.observable(MusicLibraries.ID);
    this.Title = ko.observable(MusicLibraries.Title);
    this.Artist = ko.observable(MusicLibraries.Artist);
    this.ReleaseDate = ko.observable(MusicLibraries.ReleaseDate);
    this.Genre = ko.observable(MusicLibraries.Genre);
    this.AlbumSize = ko.observable(MusicLibraries.AlbumSize);
}