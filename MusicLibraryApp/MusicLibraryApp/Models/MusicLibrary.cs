﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicLibraryApp.Models
{
    public class MusicLibrary
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string ReleaseDate { get; set; }
        public string Genre { get; set; }
        public int AlbumSize { get; set; }
    }

    public class MusicLibraryDBContext : DbContext
    {
        public DbSet<MusicLibrary> MusicLibraries { get; set; }
    }
}