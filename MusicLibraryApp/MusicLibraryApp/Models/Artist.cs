﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicLibraryApp.Models
{
    public class Artist
    {
        public int artistID { get; set; }
        public String artistName { get; set; }
        public String artistAge { get; set; }
        public int artistRank { get; set; }

    }
}