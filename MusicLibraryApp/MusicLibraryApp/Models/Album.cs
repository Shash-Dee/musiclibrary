﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicLibraryApp.Models
{
    public class Album
    {
        public int albumID { get; set; }
        public String albumTitle { get; set; }
        public DateTime releasedYear { get; set; }
    }
}