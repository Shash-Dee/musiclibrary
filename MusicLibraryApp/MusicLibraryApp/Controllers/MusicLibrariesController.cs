﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MusicLibraryApp.Models;

namespace MusicLibraryApp.Controllers
{
    public class MusicLibrariesController : Controller
    {
        private MusicLibraryDBContext db = new MusicLibraryDBContext();

        // GET: MusicLibraries
        public ActionResult Index()
        {
            return View(db.MusicLibraries.ToList());
        }

        // GET: MusicLibraries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MusicLibrary musicLibrary = db.MusicLibraries.Find(id);
            if (musicLibrary == null)
            {
                return HttpNotFound();
            }
            return View(musicLibrary);
        }

        // GET: MusicLibraries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MusicLibraries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public String Create(MusicLibrary musicLibrary)
        {
            if (ModelState.IsValid)
            {
                db.MusicLibraries.Add(musicLibrary);
                db.SaveChanges();
                return "success";
            }

            return null;
        }

        // GET: MusicLibraries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MusicLibrary musicLibrary = db.MusicLibraries.Find(id);
            if (musicLibrary == null)
            {
                return HttpNotFound();
            }
            return View(musicLibrary);
        }

        // POST: MusicLibraries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Artist,ReleaseDate,Genre,AlbumSize")] MusicLibrary musicLibrary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(musicLibrary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(musicLibrary);
        }

        // GET: MusicLibraries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MusicLibrary musicLibrary = db.MusicLibraries.Find(id);
            if (musicLibrary == null)
            {
                return HttpNotFound();
            }
            return View(musicLibrary);
        }

        // POST: MusicLibraries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MusicLibrary musicLibrary = db.MusicLibraries.Find(id);
            db.MusicLibraries.Remove(musicLibrary);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public JsonResult FillIndex()
        {
            return Json(db.MusicLibraries.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
